import express from 'express'
import http from 'http'
import cors from 'cors'
import { Server } from 'socket.io'

const app = express()
const server = http.createServer(app)
const io = new Server(server, {
    cors: {
        origin: "*"
    }
})
const PORT = 3000 || process.env.PORT

app.use(cors())

io.on("connection", socket => {
    console.log('a user connected')

    socket.on("message", (msg) => {
        let date: Date = new Date()
        io.emit("message", `${date.getHours()}:${date.getMinutes()}|${socket.id} >> ${msg}`)
    })

    socket.on('disconnect', () => {
        console.log('user disconnected')
    })
})

server.listen(PORT, () => {
    console.log(`Je fait du voyeurisme sur le port ${PORT}`)
})
