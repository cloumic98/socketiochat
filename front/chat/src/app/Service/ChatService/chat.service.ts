import { Injectable } from '@angular/core';
import {WebSocketService} from "../WebSocket/web-socket.service";

@Injectable({
  providedIn: 'root'
})
export class ChatService {

  constructor(private webSocket: WebSocketService) { }

  sendMsg(msg: string): void {
    this.webSocket.sendMessage(msg)
  }

  registerChannel(channel: string, callback: { (msg: any): void; (...args: any[]): void; }): void {
    this.webSocket.socket.on(channel, callback)
  }
}
