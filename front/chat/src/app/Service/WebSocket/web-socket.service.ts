import { Injectable } from '@angular/core';
import { io, Socket} from 'socket.io-client';
import {environment} from "../../../environments/environment";
import {DefaultEventsMap} from '@socket.io/component-emitter';

@Injectable({
  providedIn: 'root'
})
export class WebSocketService {

  socket: Socket<DefaultEventsMap, DefaultEventsMap>;

  constructor() {
    this.socket = io(environment.SOCKET_ENDPOINT)
  }

  sendMessage(message: string): void {
    this.socket.emit("message", message)
  }
}
