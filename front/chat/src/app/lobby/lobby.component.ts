import { Component, OnInit } from '@angular/core';
import {ChatService} from "../Service/ChatService/chat.service";
import {FormBuilder, FormGroup} from "@angular/forms";

@Component({
  selector: 'app-lobby',
  templateUrl: './lobby.component.html',
  styleUrls: ['./lobby.component.css']
})
export class LobbyComponent implements OnInit {

  // @ts-ignore
  myForm: FormGroup

  messages: string[] = []

  constructor(private cService: ChatService, private fb: FormBuilder) {
    this.cService.registerChannel("message", (msg: any) => {
      this.onMessage(msg)
    })
  }

  sendMessage() {
    this.cService.sendMsg(this.myForm.value.message)
    this.myForm.reset({
      message: ''
    })
  }

  onMessage(message: any) {
    this.messages.push(message)
  }

  ngOnInit(): void {
    this.myForm = this.fb.group({
      message: ''
    })
  }
}
